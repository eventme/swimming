<?php
/**
 * Template name: Uniform
 *
 */
get_header();
?>

<?php
$pages = get_pages(array(
    'meta_key' => '_wp_page_template',
    'meta_value' => 'template-uniform.php'
));
$page_id = '';
foreach ($pages as $page) {
    $page_id = $page->ID;

}
?>


<?php while (have_posts()) : the_post(); ?>
    <div class="wrapper">
        <div class="page-wrap uniform">
            <div class="container">
                <h2 class="caption-border">
                    <?php echo get_the_title($page_id) ?>
                </h2>
                <!-- /.caption-border -->
                <?php the_content(); ?>

                <div class="uniform-wrap">


                    <?php $uniforms = carbon_get_post_meta($page_id, 'crb_uniform_images');
                    foreach ($uniforms as $uniform): ?>

                            <?php $uniform_src = wp_get_attachment_image_src($uniform, 'full')[0]; ?>
                            <div class="uniform-item">
                                <a href="http://www.swimkit.ie/swan-leisure/">
                                    <img src="<?php echo $uniform_src; ?>" alt="image">
                                </a>

                            </div>


                        <?php endforeach; ?>
                </div>
                <!-- /.uniform-wrap -->
            </div>
        </div>
    </div>

<?php endwhile; ?>
<?php get_footer();