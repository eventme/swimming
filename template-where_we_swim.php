<?php
/**
 * Template name: Where we swim
 *
 */
get_header();
?>


<?php
$pages = get_pages(array(
    'meta_key' => '_wp_page_template',
    'meta_value' => 'template-main_page.php'
));
$page_id = '';
foreach ($pages as $page) {
    $page_id = $page->ID;

}
?>


<?php while (have_posts()) : the_post(); ?>
    <div class="wrapper">
        <div class="page-wrap">
            <div class="container">
                <div class="about-us">
                    <h2 class="caption-border">
                        <?php the_title(); ?>
                    </h2>
                    <div class="contact-info">
                        <div class="location">
                            <h3><?php echo carbon_get_post_meta(get_the_ID(), 'crb_swimm_title'); ?></h3>
                            <p><?php echo carbon_get_post_meta(get_the_ID(), 'crb_swimm_text'); ?></p>
                            <?php $mail = carbon_get_post_meta(get_the_ID(), 'crb_swimm_mail'); ?>
                            <a href='maito:<?php echo $mail; ?>'><?php echo $mail; ?></a>
                        </div>
                        <div class="photo-pool">
                            <img src="<?php echo carbon_get_post_meta(get_the_ID(), 'crb_image_swimming_pool'); ?>"
                                 alt="image">
                        </div>
                        <!-- /.photo-pool -->
                    </div>
                </div>
                <!-- /.about-us -->
            </div>
            <!-- /.container -->
            <div id="map" class="map"></div> <!-- /#map -->
        </div>
        <!-- /.page-wrap -->
    </div>
    <!-- /.wrapper -->

<?php endwhile; ?>


<?php
get_footer();

