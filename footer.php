<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package swimming
 */

?>


<footer>
    <div class="container">
        <div class="footer-wrap">
            <div class="footer-logo">
                <?php if( get_theme_mod( 'custom_footer_logo') != "" ): ?>
                    <a href="<?php echo home_url(); ?>">
                        <img src="<?php echo get_theme_mod( 'custom_footer_logo'); ?>" alt="image">
                    </a>
                <?php endif; ?>
            </div>
            <div class="footer-nav">
            <?php 
                wp_nav_menu( array(
                    'theme_location'  => 'footer_1',
                    'fallback_cb'     => '__return_empty_string',
                    'container'       => false, 
                    'menu_class'      => 'footer-navigation-item footer-nav-top', 
                    'menu_id'         => '', 
                    'echo'            => true,
                    'fallback_cb'     => 'wp_page_menu',
                    'link_before'     => '',
                    'link_after'      => '',
                    'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                ) ); 

                wp_nav_menu( array(
                    'theme_location'  => 'footer_2',
                    'fallback_cb'     => '__return_empty_string',
                    'container'       => false, 
                    'menu_class'      => 'footer-navigation-item footer-nav-top', 
                    'menu_id'         => '', 
                    'echo'            => true,
                    'fallback_cb'     => 'wp_page_menu',
                    'link_before'     => '',
                    'link_after'      => '',
                    'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                ) ); 
                wp_nav_menu( array(
                    'theme_location'  => 'footer_1',
                    'fallback_cb'     => '__return_empty_string',
                    'container'       => false, 
                    'menu_class'      => 'footer-navigation-item', 
                    'menu_id'         => '', 
                    'echo'            => true,
                    'fallback_cb'     => 'wp_page_menu',
                    'link_before'     => '',
                    'link_after'      => '',
                    'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                ) ); 
                wp_nav_menu( array(
                    'theme_location'  => 'footer_1',
                    'fallback_cb'     => '__return_empty_string',
                    'container'       => false, 
                    'menu_class'      => 'footer-navigation-item', 
                    'menu_id'         => '', 
                    'echo'            => true,
                    'fallback_cb'     => 'wp_page_menu',
                    'link_before'     => '',
                    'link_after'      => '',
                    'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                ) ); 
            ?>
            </div>
            <!-- /.footer-nav -->

            <!-- /.footer-logo -->
        </div>
        <div class="confidentiality">
            <p>Swan Swimming Club™. All rights Reserved.</p>
            <div id="scroll-top" class="scroll-top">
                <a href="#"><svg xmlns="http://www.w3.org/2000/svg" width="8" height="22" viewBox="0 0 8 22" ratio="1"><polygon points="8,5 4,0 0,5 3.497,5 3.497,22 4.504,22 4.504,5 "></polygon></svg></a>
            </div>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
