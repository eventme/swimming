<?php
/**
 * Template name: Committee
 *
 */
get_header();
?>


<?php
$pages = get_pages(array(
    'meta_key' => '_wp_page_template',
    'meta_value' => 'template-committee.php'
));
$page_id = '';
foreach ($pages as $page) {
    $page_id = $page->ID;
}

$members = carbon_get_post_meta($page_id, 'crb_association'); ?>

<?php while (have_posts()) : the_post(); ?>

    <div class="wrapper">
        <div class="committee-wrap">
            <div class="container">
                <div class="committee">
                    <h2 class="caption-border">
                        <?php the_title(); ?>
                    </h2>

                    <?php the_content(); ?>

                </div>
                <!-- /.committee -->


                <div class="members-b clearfix">
                    <div class="members-b__title"><?php echo carbon_get_post_meta(get_the_ID(), 'crb_users_block_title'); ?></div>
                    <?php $users = carbon_get_post_meta(get_the_ID(), 'crb_users_block');
                    foreach ($users as $user) { ?>

                        <div class="members-item">
                            <img class="members-item__photo" src="<?php echo $user['crb_users_block_foto']; ?>"
                                 alt="avatar">
                            <p class="members-item__name"><?php echo $user['crb_users_block_name']; ?></p>
                            <p class="members-item__profession"><?php echo $user['crb_users_block_profession']; ?></p>
                            <a href="maito:<?php echo $user['crb_users_block_email']; ?>"
                               class="members-item__mail"><?php echo $user['crb_users_block_email']; ?></a>
                        </div>

                    <?php } ?>
                </div>
                <p><?php echo carbon_get_post_meta(get_the_ID(), 'crb_others_committee'); ?></p>
                <br>
                <p><?php echo carbon_get_post_meta(get_the_ID(), 'crb_feedback'); ?></p>
                <!--        biography-person-->


            </div>
        </div>
        <!-- /.committee-wrap -->

        <!--    social-->
    </div>
    <!--wrapper-->

<?php endwhile; ?>

<?php

get_footer();
