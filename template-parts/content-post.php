<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package swimming
 */

?>
<div class="wrapper">
    <div class="page-wrap">
        <div class="container">
            <!-- /.container -->
            <!-- /.page-wrap -->
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


                <!-- /.wrapper -->
                <div class="entry-content">
                    <div class="news-block">
                        <div class="news-info">
                            <h3><?php the_title() ?></h3>
                            <p>
                                <span class="date-news"><?php the_date() ?></span>
                            </p>
                        </div>
                        <div class="news-wrap">
                            <div class="news-photo">
                                <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="image">
                            </div>
                            <div class="news-text">
                                <?php
                                the_content();
                                ?>
                            </div>
                        </div>
                    </div>
                </div>


            </article>
        </div>
    </div>
</div>