<?php
/**
 * Template name: FAQ
 *
 */
get_header();
?>
    <div class="wrapper">
        <div class="page-wrap">
            <div class="container">
                <h2 class="caption-border">
                    <?php the_title(); ?>
                </h2>
                <!-- /.caption-border -->
                <?php echo do_shortcode('[hrf_faqs]'); ?>
            </div>
        </div>
    </div>

<?php
get_footer();
