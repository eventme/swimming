<?php
/**
 * Template name: Contact Us
 *
 */
get_header(); ?>

    <div class="wrapper">
        <div id="map" class="map"></div>
        <div class="page-wrap">
            <div class="container">
                <div class="contact-info">
                    <div class="contact-form">
                        <h2><?php the_title(); ?></h2>

                        <?php echo do_shortcode('[contact-form-7 id="13" title="Contact form 1"]'); ?>

                    </div>
                    <div class="location">
                        <h3><?php echo carbon_get_post_meta(get_the_ID(), 'crb_swimm_title'); ?></h3>
                        <p><?php echo carbon_get_post_meta(get_the_ID(), 'crb_swimm_text'); ?></p>
                        <?php $mail = carbon_get_post_meta(get_the_ID(), 'crb_swimm_mail'); ?>
                        <a href='maito:<?php echo $mail; ?>'><?php echo $mail; ?></a>
                        <hr class='dots'>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.wrapper -->

<?php get_footer(); ?>