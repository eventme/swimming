<?php
/**
 * Template name: News
 *
 */
get_header();


$args = array(
    'post_type' => 'post',
    'post_per_page' => 3
);
$query = new WP_Query($args);
?>

    <div class="wrapper">
        <div class="page-wrap news-items">
            <div class="container">
                <h2 class="caption-border">
                    <?php echo get_the_title($page_id) ?>
                </h2>
                <!-- /.caption-border -->
                <?php
                if ($query->have_posts()) {

                    // The 2nd Loop
                    while ($query->have_posts()) {
                        $query->the_post(); ?>

                        <div class="news-block">
                            <div class="news-info">
                                <h3><a href="<?php the_permalink(); ?>">
                                        <?php the_title(); ?>
                                    </a></h3>
                                <p>
                                <span class="date-news">
<!--                                    --><?php //the_date('Y-m-d'); ?>
                                </span>
                                    <!-- /.date-news -->
                                </p>
                            </div>
                            <!-- /.news-info -->
                            <div class="news-wrap">
                                <div class="news-photo">
                                    <a href="<?php the_permalink(); ?>">
                                        <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="image">
                                    </a>
                                </div>
                                <!-- /.news-photo -->
                                <div class="news-text">
                                    <?php the_excerpt(); ?>
                                </div>
                                <!-- /.news-text -->

                            </div>
                            <!-- /.news-wrap -->
                        </div>
                        <!-- /.news-block -->


                    <?php }
                    wp_reset_postdata();
                }


                ?>

            </div>
            <!-- /.container -->
        </div>
        <!-- /.page-wrap -->
    </div>
    <!--wrapper-->
<?php
get_footer();
