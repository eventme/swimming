<?php

/**
 * Create Logo Setting and Upload Control
 */
add_action( 'customize_register', 'swimming_register_theme_customizer' );
/*
 * Register Our Customizer Stuff Here
 */
function swimming_register_theme_customizer( $wp_customize ) {
    // Create custom panel.
    $wp_customize->add_panel( 'text_blocks', array(
        'priority'       => 500,
        'theme_supports' => '',
        'title'          => __( 'Footer', 'swimming' )
    ) );
    // Add Footer Text
    // Add section.
    $wp_customize->add_section( 'custom_text' , array(
        'title'    => __('Footer Logo','swimming'),
        'panel'    => 'text_blocks',
        'priority' => 10
    ) );

    $wp_customize->add_setting( 'custom_footer_logo', array(
        'default' => '',
    ) );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'custom_footer_logo', array(
        'label'      => 'Footer logo',
        'section'    => 'custom_text',
        'settings'   => 'custom_footer_logo',
    ) ) );

    // Sanitize text
    function sanitize_text( $text ) {
        return sanitize_text_field( $text );
    }
}