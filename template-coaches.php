<?php
/**
 * Template name: Coaches
 *
 */
get_header();
?>


<?php
$pages = get_pages(array(
    'meta_key' => '_wp_page_template',
    'meta_value' => 'template-coaches.php'
));
$page_id = '';
foreach ($pages as $page) {
    $page_id = $page->ID;
}

get_the_title($page_id);

get_post_field('post_content', $page_id);

$members = carbon_get_post_meta($page_id, 'crb_association_coaches');

?>


    <div class="wrapper">
            <div class="committee-wrap">
                <div class="container">
                    <div class="committee">
                        <h2 class="caption-border">
                            <?php echo get_the_title($page_id); ?>
                        </h2>
                        <p>
                            <?php echo get_post_field('post_content', $page_id); ?>
                        </p>
                    </div>
                    <!-- /.committee -->
                </div>
            </div>
            <!-- /.committee-wrap -->
<?php

foreach ($members as $member) {
    $name = carbon_get_post_meta($member['id'], 'crb_member_name');
    $profession = carbon_get_post_meta($member['id'], 'crb_member_profession');
    $qualification = carbon_get_post_meta($member['id'], 'crb_member_qualification');

    ?>

            <div class="biography-person">
                <div class="container">
                    <div class="members-committee">
                        <h2 class="caption-border">
                            <?php echo get_the_title($member['id']); ?>
                        </h2>

                        <div class="photo-person">
                        <?php echo get_the_post_thumbnail($member['id']);
                        ?>
                            <div class="contacts">
<!--                                <p class="members-name">--><?php //echo get_the_title($member['id']); ?><!--</p>-->
                                <p class='number-profession'><?php echo $profession ?></p>
                                <p class="members-qualification"><?php echo $qualification ?></p>
                            </div>
<!--                            <div class="social">-->
<!--                        --><?php
//
//                        $socials = carbon_get_post_meta($member['id'], 'crb_member_social');
//                        foreach ($socials as $social) {
//                            $social_url = $social['member_social_url'];
//                            $social_image_url = $social['member_social_image'];
//                            ?>
<!--                                <a href="--><?php //echo $social_url ?><!--"><img src="--><?php //echo $social_image_url ?><!-- " alt="image"> </a>-->
<!--                            --><?php
//
//                        }
//                        ?>
<!---->
<!---->
<!---->
<!--</div>-->
                        </div>
                        <!--    photo-person-->
                        <div class="about-person">
<!--                            <h3>-->
<!--                                --><?php //echo carbon_get_post_meta($member['id'], 'crb_member_name') ?>
<!--                            </h3>-->
                                <p>
                                    <?php echo get_post_field('post_content', $member['id']); ?>
                                </p>
                        </div>
                        <!-- /.about-person -->
                    </div>
<!--                    members-committee-->
                 </div>
<!--                container-sm-->
            </div>
    <?php } ?>
<!--        biography-person-->
                        <!--    social-->
   </div>
<!--wrapper-->


<?php

get_footer();
