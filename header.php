<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package swimming
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<header id="header">
    <div class="container">
        <div class="header-wrap">
            <!-- /.header-wrap -->

            <?php wp_nav_menu(array('theme_location' => 'menu-1', 'menu_class' => 'navigation')); ?>

            <div class="hamburger">
                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" ratio="1">
                    <rect y="9" width="20" height="2"></rect>
                    <rect y="3" width="20" height="2"></rect>
                    <rect y="15" width="20" height="2"></rect>
                </svg>
            </div>
        </div><!-- /.logo -->
    </div>

    <div class="header-logo">
        <div class="container">
            <div class="header-name-club">
                <div class="logo">
                    <a href="/">
                        <?php
                        //                    $custom_logo_id = get_theme_mod('custom_logo');
                        $custom_logo_id = get_theme_mod('custom_logo');
                        $image = wp_get_attachment_image_src($custom_logo_id, 'full');
                        ?>
                        <img src="<?php echo $image[0]; ?>" alt="image">
                    </a>
                </div>
                <h1>
                    Swan Leisure Swim Club
                </h1>
                <!-- /.h1 -->
            </div>
            <!-- /.header-name-club -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.header-logo -->

    <div class="sm-navigation">
        <!-- /.sm-navigation -->
        <div class="close">
            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14" ratio="1">
                <line fill="none" stroke="#000" stroke-width="1.1" x1="1" y1="1" x2="13" y2="13"></line>
                <line fill="none" stroke="#000" stroke-width="1.1" x1="13" y1="1" x2="1" y2="13"></line>
            </svg>
        </div>
        <?php wp_nav_menu(array('theme_location' => 'menu-1', 'menu_class' => 'navigation')); ?>
    </div>
    <!-- /.container -->
</header>




