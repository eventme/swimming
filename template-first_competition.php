<?php
/**
 * Template name: First competition
 *
 */
get_header();
?>

<?php while (have_posts()) : the_post(); ?>
    <div class="wrapper">
        <div class="page-wrap">
            <div class="container">
                <div class="first-competition">
                    <h2 class="caption-border">
                        <?php echo get_the_title($page_id); ?>
                    </h2>

                    <div class="text-item">
                        <?php the_content(); ?>
                        <div class="accordeon-content">
                            <?php $accordeons = carbon_get_post_meta(get_the_ID(), 'crb_accordeon');
                            foreach ($accordeons as $accordeon) {
                                ?>
                                <div class="accordeon-item">
                                    <h3>
                                        <?php echo $accordeon['crb_accordeon_row_1']; ?>
                                    </h3>
                                    <p>
                                        <?php echo $accordeon['crb_accordeon_row_2']; ?>
                                    </p>
                                </div>
                                <!-- /.accordeon-item -->
                            <?php  }
                            ?>
                        </div>
                        <!-- /.accordeon-content -->
                    </div>
                    <!-- /.text-item -->




                </div>
                <!-- /.first-competition -->
            </div>
        </div>
    </div>

<?php endwhile; ?>

<?php get_footer();