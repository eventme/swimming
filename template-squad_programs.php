<?php
/**
 * Template name: Sqaud programs
 *
 */
get_header();
?>
<?php while (have_posts()) : the_post(); ?>
    <div class="wrapper">
        <div class="page-wrap">
            <div class="container">
                <div class="squad-programs">
                    <h2 class="caption-border">
                        <?php echo get_the_title($page_id); ?>
                    </h2>
                    <div class="text-item">
                        <?php the_content(); ?>
                        <div class="accordeon-content accordeon-squad">
                            <?php $accordeons_2 = carbon_get_post_meta(get_the_ID(), 'crb_accordeon_2');
                            foreach ($accordeons_2 as $accordeon_2) {
                                ?>

                                <div class="accordeon-item">
                                    <h3>
                                        <?php echo $accordeon_2['crb_accordeon_list_1']; ?>
                                    </h3>
                                    <h4>Description</h4>
                                    <p>
                                        <?php echo $accordeon_2['crb_accordeon_list_2']; ?>
                                    </p>
                                    <h4>Criteria</h4>
                                    <p>
                                        <?php echo $accordeon_2['crb_accordeon_list_3']; ?>
                                    </p>
                                </div>
                                <!-- /.accordeon-item -->
                            <?php  }
                            ?>
                        </div>
                        <!-- /.accordeon-content -->
                    </div>
                    <!-- /.text-block -->
                </div>
                <!-- /.squad-programs -->
            </div>
        </div>
    </div>

<?php endwhile; ?>
<?php get_footer();