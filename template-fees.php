<?php
/**
 * Template name: Fees
 *
 */

get_header(); ?>

    <div class="wrapper">
        <?php while (have_posts()) : the_post(); ?>
            <div class="page-wrap">
                <div class="container">
                    <h2 class="caption-border">
                        <?php echo get_the_title($page_id); ?>
                    </h2>
                    <!-- /.caption-border -->
                    <div class="fees-text"><?php the_content(); ?></div>

                    <div class="price-table">
                        <p class="column-header">
                            <?php echo carbon_get_post_meta(get_the_ID(), 'crb_fees_table_title'); ?>
                        </p>
                        <?php $rows = carbon_get_post_meta(get_the_ID(), 'crb_fees_table');
                        $count = 1;
                        foreach ($rows as $row) { ?>

                            <?php if ($count == 1): ?>
                                <div class="column-header">
                                    <p class="squad"><?php echo $row['crb_fees_column_1']; ?></p>
                                    <p class="lessons"><?php echo $row['crb_fees_column_2']; ?></p>
                                    <p class="price"><?php echo $row['crb_fees_column_3']; ?></p>
                                </div>
                            <?php else: ?>
                                <div class="table-item">
                                    <p class="table-item-squad"><?php echo $row['crb_fees_column_1']; ?></p>
                                    <p class="table-item-lesson"><?php echo $row['crb_fees_column_2']; ?></p>
                                    <p class="table-item-price"><?php echo $row['crb_fees_column_3']; ?></p>
                                </div>
                            <?php endif; ?>

                            <?php $count++;
                        } ?>
                        <!-- /.table-item -->
                    </div>
                    <!-- /.price-table -->
                </div>
                <!-- /.container -->
            </div>
            <!-- /.page-wrap -->
        <?php endwhile; ?>
    </div>
    <!-- /.wrapper -->


<?php
get_footer();
