<?php
/**
 * Template name: Poolsitting-duties
 *
 */
get_header(); ?>

<?php
$pages = get_pages(array(
    'meta_key' => '_wp_page_template',
    'meta_value' => 'template-poolsitting_duties.php'
));
$page_id = '';
foreach ($pages as $page) {
    $page_id = $page->ID;

}
?>

<?php while (have_posts()) : the_post(); ?>

    <div class="wrapper">
        <div class="page-wrap">
            <!-- /.page-wrap -->
            <div class="container">
                <h2 class="caption-border">
                    <?php echo get_the_title($page_id); ?>
                </h2>
                <div class="important-info">
                    <p><?php echo carbon_get_post_meta($page_id, 'crb_important-info') ?></p>
                </div>
                <!-- /.important-info -->
                <div class="behavior-rules">
                    <?php the_content(); ?>
                </div>
                <!-- /.behavior-rules -->
                <?php $files = carbon_get_post_meta($page_id, 'crb_download_files');
//              var_dump($files);
                foreach ( $files as $file ) { ?>

                    <?php $file_data = get_post( $file['crb_download-btn'] );
                    ?>

                    <div class="download-file">
                        <div class="download-img">
                            <img src="<?php echo bloginfo('template_url') ?>/assets/img/download-to-storage-drive.svg"
                                 alt="image">
                        </div>
                        <!-- /.download-img -->
                        <div class="download-file-name">
                            <p><?php echo $file_data->post_title; ?></p>
                        </div>
                        <!-- /.download-file-name -->
                        <div class="download-btn">
                            <a href="<?php echo $file_data->guid; ?>"><?php echo __('Download'); ?></a>
                        </div>
                        <!-- /.download-btn -->

                    </div>
                    <!-- /.download-file -->

                <?php } ?>

            </div>
            <!-- /.container -->
        </div>
        <!--        page-wrap-->
    </div>
    <!-- /.wrapper -->

<?php endwhile; ?>

<?php get_footer();