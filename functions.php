<?php
/**
 * swimming functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package swimming
 */


/**
 * Customizer additions.
 */
//require get_template_directory() . '/inc/customizer.php';


function my_myme_types($mime_types)
{
    $mime_types['svg'] = 'image/svg+xml'; // поддержка SVG
    return $mime_types;
}

add_filter('upload_mimes', 'my_myme_types', 1, 1);


if (!function_exists('swimming_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function swimming_setup()
    {
        load_theme_textdomain('swimming', get_template_directory() . '/languages');
        add_theme_support('automatic-feed-links');
        add_theme_support('title-tag');
        add_theme_support('post-thumbnails');


        register_nav_menus(array(
            'menu-1' => esc_html__('Primary', 'swimming'),

        ));
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        add_theme_support('custom-background', apply_filters('swimming_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        )));

        add_theme_support('customize-selective-refresh-widgets');

        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        add_theme_support('custom-logo', array(
            'height' => 250,
            'width' => 250,
            'flex-width' => true,
            'flex-height' => true,
        ));
    }
endif;
add_action('after_setup_theme', 'swimming_setup');

function swimming_content_width()
{
    $GLOBALS['content_width'] = apply_filters('swimming_content_width', 640);
}

add_action('after_setup_theme', 'swimming_content_width', 0);

//function swimming_widgets_init() {
//	register_sidebar( array(
//		'name'          => esc_html__( 'Sidebar', 'swimming' ),
//		'id'            => 'sidebar-1',
//		'description'   => esc_html__( 'Add widgets here.', 'swimming' ),
//		'before_widget' => '<section id="%1$s" class="widget %2$s">',
//		'after_widget'  => '</section>',
//		'before_title'  => '<h2 class="widget-title">',
//		'after_title'   => '</h2>',
//	) );
//}
//add_action( 'widgets_init', 'swimming_widgets_init' );

/**
 * Enqueue scripts and styles.
 */


function new_excerpt_length($length)
{
    return 100;
}

add_filter('excerpt_length', 'new_excerpt_length');

add_filter('excerpt_more', function ($more) {
    return '...';
});
//add_filter('excerpt_more', 'new_excerpt_more');
//function new_excerpt_more($more) {
//    global $post;
//    return '<a href="'. get_permalink($post->ID) . '" class="read-more">Читать дальше</a>';
//}


function swimming_scripts()
{
    wp_enqueue_style('swimming-style', get_stylesheet_uri());
    wp_enqueue_style('swimming-style_mfp', 'https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css' . '');
    wp_enqueue_style('swimming-style_main', get_template_directory_uri() . '/assets/css/style.min.css');


    wp_enqueue_script('swimming-map', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAzEreQz0-m-oznUbXs1cUpkRwbooOe-Wg', array(), time(), true);
    wp_enqueue_script('swimming-main_script', get_template_directory_uri() . '/assets/js/script.min.js', array(), time(), true);
    wp_enqueue_script('swimming-mfp_script', 'https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js', array(), time(), true);
    wp_enqueue_script('swimming-header', get_template_directory_uri() . '/assets/js/header.js', array(), time(), true);

}


/**
 * Enqueue scripts for Google Map location data.
 */
add_action('wp_enqueue_scripts', 'swimming_map_data_scripts');
function swimming_map_data_scripts()
{
    $pages = get_pages(array(
        'meta_key' => '_wp_page_template',
        'meta_value' => 'template-contact_us.php'
    ));
    $page_id = '';
    foreach ($pages as $page) {
        $page_id = $page->ID;
    }
    $data = carbon_get_post_meta($page_id, 'crb_swimm_map');
    wp_localize_script('jquery', 'map_location', $data);
}


use Carbon_Fields\Field;
use Carbon_Fields\Container;

add_action('wp_enqueue_scripts', 'swimming_scripts');
add_action('carbon_fields_register_fields', 'crb_attach_theme_options');
function crb_attach_theme_options()
{

    Container::make('post_meta', __('Custom Data', 'crb'))
        ->where(function ($condition) {
            $condition->where('post_type', '=', 'page');
            $condition->where('post_template', '=', 'template-main_page.php');
        })
        ->add_fields([
            Field::make('image', 'crb_swim_club_logo', 'Club logo')
                ->set_value_type('url')->set_width(100),

            Field::make('complex', 'crb_swim_slider', 'Slider')
                ->set_layout('tabbed-horizontal')
                ->add_fields([
                    Field::make('image', 'crb_swim_foto', 'Photo')
                        ->set_value_type('url')->set_width(33),
                    Field::make('text', 'crb_slider_text_1', 'Text 1')->set_width(33),
                    Field::make('text', 'crb_slider_text_2', 'Text 2')->set_width(33),
                ]),

            Field::make('complex', 'crb_swim_block', 'Second Block')
                ->set_layout('tabbed-horizontal')
                ->add_fields([
                    Field::make('image', 'crb_block_foto', 'Photo')
                        ->set_value_type('url')->set_width(33),
                    Field::make('text', 'crb_block_title', 'Title')->set_width(33),
                    Field::make('text', 'crb_block_link', 'Url')->set_width(33),
                ]),

            Field::make('media_gallery', 'crb_masonry_gallery', 'slider')
                ->set_type(array('image', 'video')),


            Field::make('text', 'crb_link_title', 'Link Title')->set_width(50),
            Field::make('text', 'crb_link_url', 'Link URL')->set_width(50),


        ]);


    Container::make('post_meta', __('Custom Data', 'crb'))
        ->where(function ($condition) {
            $condition->where('post_type', '=', 'page');
            $condition->where('post_template', '=', 'template-uniform.php');
        })
        ->add_fields([
            Field::make('media_gallery', 'crb_uniform_images', 'Uniform')
                ->set_type(array('image', 'video'))
        ]);


    Container::make('post_meta', __('Custom Data', 'crb'))
        ->where(function ($condition) {
            $condition->where('post_type', '=', 'page');
            $condition->where('post_template', '=', 'template-fees.php');
        })
        ->add_fields([
            Field::make('text', 'crb_fees_table_title', 'Title Table'),
            Field::make('complex', 'crb_fees_table', 'Table')
                ->set_layout('tabbed-horizontal')
                ->add_fields([
                    Field::make('text', 'crb_fees_column_1', 'Squad')->set_width(33),
                    Field::make('text', 'crb_fees_column_2', 'Lesson')->set_width(33),
                    Field::make('text', 'crb_fees_column_3', 'Price')->set_width(33),
                ]),

        ]);

    Container::make('post_meta', __('Custom Data', 'crb'))
        ->where(function ($condition) {
            $condition->where('post_type', '=', 'page');
            $condition->where('post_template', '=', 'template-first_competition.php');
        })
        ->add_fields([
            Field::make('complex', 'crb_accordeon', 'Accordeon')
                ->set_layout('tabbed-horizontal')
                ->add_fields([
                    Field::make('text', 'crb_accordeon_row_1', 'Competition heading')->set_width(33),
                    Field::make('textarea', 'crb_accordeon_row_2', 'Competition text')->set_width(33),
                ]),

        ]);


    Container::make('post_meta', __('Custom Data', 'crb'))
        ->where(function ($condition) {
            $condition->where('post_type', '=', 'page');
            $condition->where('post_template', '=', 'template-squad_programs.php');
        })
        ->add_fields([
            Field::make('complex', 'crb_accordeon_2', 'Accordeon')
                ->set_layout('tabbed-horizontal')
                ->add_fields([
                    Field::make('text', 'crb_accordeon_list_1', 'Squad heading'),
                    Field::make('textarea', 'crb_accordeon_list_2', 'Description'),
                    Field::make('rich_text', 'crb_accordeon_list_3', 'Criteria')
                ]),

        ]);


    Container::make('post_meta', __('Custom Data', 'crb'))
        ->where(function ($condition) {
            $condition->where('post_type', '=', 'page');
            $condition->where('post_template', '=', 'template-poolsitting_duties.php');
        })
        ->add_fields([
            Field::make("textarea", "crb_important-info", "Important info")->set_width(33),
            Field::make("text", "crb_behavior-rules", "Behavior rules"),

            Field::make('complex', 'crb_download_files', 'Download Files')
                ->set_layout('tabbed-horizontal')
                ->add_fields([
                    Field::make("file", "crb_download-btn", "Download files"),
                ]),
        ]);


    Container::make('post_meta', __('Custom Data', 'crb'))
        ->where(function ($condition) {
            $condition->where('post_type', '=', 'page');
            $condition->where('post_template', '=', 'template-contact_us.php');
        })
        ->add_fields([
            Field::make("text", "crb_swimm_title", "Title block"),
            Field::make("textarea", "crb_swimm_text", "Text block"),
            Field::make("text", "crb_swimm_mail", "Email"),
            Field::make("map", "crb_swimm_map", "Location")
                ->help_text('drag and drop the pin on the map to select location'),

        ]);

    Container::make('post_meta', __('Custom Data', 'crb'))
        ->where(function ($condition) {
            $condition->where('post_type', '=', 'page');
            $condition->where('post_template', '=', 'template-where_we_swim.php');
        })
        ->add_fields([
            Field::make("text", "crb_swimm_title", "Title block"),
            Field::make("textarea", "crb_swimm_text", "Text block"),
            Field::make("text", "crb_swimm_mail", "Email"),

            Field::make("image", "crb_image_swimming_pool", "Photo swimming pool")
                ->set_value_type('url')
        ]);


    Container::make('post_meta', __('Custom Data', 'crb'))
        ->where(function ($condition) {
            $condition->where('post_type', '=', 'page');
            $condition->where('post_template', '=', 'template-committee.php');
        })
        ->add_fields([
            Field::make('text', 'crb_users_block_title', 'Title Block'),
            Field::make('complex', 'crb_users_block', 'Users')
                ->set_layout('tabbed-horizontal')
                ->add_fields([
                    Field::make('image', 'crb_users_block_foto', 'Photo')
                        ->set_value_type('url')->set_width(25),
                    Field::make('text', 'crb_users_block_name', 'Full Name')->set_width(25),
                    Field::make('text', 'crb_users_block_profession', 'Profession')->set_width(25),
                    Field::make('text', 'crb_users_block_email', 'E-mail')->set_width(25),
                ]),
            Field::make('textarea', 'crb_others_committee'),
            Field::make('textarea', 'crb_feedback')


            /*Field::make( 'association', 'crb_association' )->set_types(
                array( array( 'type' => 'post', 'post_type' => 'sw_members' ) )
            )*/

        ]);


    Container::make('post_meta', __('Custom Data', 'crb'))
        ->where(function ($condition) {
            $condition->where('post_type', '=', 'page');
            $condition->where('post_template', '=', 'template-coaches.php');
        })
        ->add_fields([

            Field::make('association', 'crb_association_coaches')->set_types(
                array(array('type' => 'post', 'post_type' => 'sw_members'))
            )


        ]);

    Container::make('post_meta', __('Custom Data', 'crb'))
        ->where(function ($condition) {
            $condition->where('post_type', '=', 'page');
            $condition->where('post_template', '=', 'template-child_protection.php');
        })
        ->add_fields([

            Field::make('association', 'crb_protection_association')->set_types(
                array(array('type' => 'post', 'post_type' => 'sw_members'))
            ),

            Field::make("file", "crb_download-btn", "Download files"),


        ]);


    Container::make('post_meta', __('Custom Data', 'crb'))
        ->where('post_type', '=', 'sw_members')
        ->add_fields([

//            Field::make('text', 'crb_member_name', 'Name')->set_width(33),
            Field::make('text', 'crb_member_profession', 'Profession')->set_width(33),
            Field::make('text', 'crb_member_qualification', 'Coach Qualification')->set_width(33),

//            Field::make('text', 'crb_member_name', 'Name')->set_width(33),

//            Field::make('complex', 'crb_member_social', 'Social Links')
//                ->add_fields(array(
//                    Field::make('image', 'member_social_image', 'Image')
//                        ->set_width(50)
//                        ->set_value_type('url')
//                        ->set_required(),
//                    Field::make('text', 'member_social_url', 'URL')
//                        ->set_width(50)
//                        ->set_required(),
//                )),


        ]);
}

function sw_register_post_type()
{
    register_post_type('sw_members',
        [
            'labels' => [
                'name' => __('Members'),
                'singular_name' => __('Member'),
            ],
            'public' => true,
            'has_archive' => true,
            'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt')
        ]
    );
}

add_action('init', 'sw_register_post_type');

add_action('carbon_fields_map_field_api_key', 'crb_get_gmaps_api_key');
function crb_get_gmaps_api_key($current_key)
{
    return 'AIzaSyAzEreQz0-m-oznUbXs1cUpkRwbooOe-Wg';
}


///* Get the location data as an array */
//carbon_get_post_meta($id, $name, 'map'); // array('lat' => 40.74866, 'lng' => -73.97982, 'address' => '45 Park Avenue,  New York, NY 10016', 'zoom' => 8)
//
///* Get the location latitude and longitude */
//carbon_get_post_meta($id, $name); // '40.74866,-73.97982'

