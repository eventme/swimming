<?php
/**
 * Template name: Nutrition
 *
 */
get_header();
?>
<?php while (have_posts()) : the_post(); ?>
    <div class="wrapper">
        <div class="page-wrap">
            <div class="container">
                <div class="nutrition">
                    <h2 class="caption-border">
                        <?php echo get_the_title($page_id); ?>
                    </h2>
                   <div class="text-item">
                       <?php the_content(); ?>
                   </div>
                   <!-- /.text-block -->

                </div>
                <!-- /.nutrition -->
            </div>
        </div>
    </div>

<?php endwhile; ?>
<?php get_footer();