<?php
/**
 * Template name: Child protection
 *
 */
get_header();
$pages = get_pages(array(
    'meta_key' => '_wp_page_template',
    'meta_value' => 'template-child_protection.php'
));
$page_id = '';
foreach ($pages as $page) {
    $page_id = $page->ID;
}

//$members = carbon_get_post_meta($page_id, 'crb_protection_association');
//
//foreach ($members as $member) {
//    $address = carbon_get_post_meta($member['id'], 'crb_member_address');
//    $tel = carbon_get_post_meta($member['id'], 'crb_member_tel');
//    $mail = carbon_get_post_meta($member['id'], 'crb_member_email');

    ?>
    <!--        пиши здесь Использовать переменную вот так echo $mail к примеру -->
    <div class="wrapper">

        <?php while ( have_posts() ) : the_post(); ?>

            <div class="page-wrap child-protection">
                <div class="container">
                    <h2 class="caption-border"><?php the_title(); ?></h2>
                        <?php the_content(); ?>
                    <!-- /.caption-border -->
                    <?php
                    $file = get_post( carbon_get_post_meta(get_the_ID(), 'crb_download-btn') );
                    ?>
                    <div class="download-file">
                        <a class="link-file" href="<?php echo $file->guid; ?>"></a>
                        <div class="download-img">
                            <img src="<?php echo bloginfo('template_url') ?>/assets/img/download-to-storage-drive.svg"
                                 alt="image">
                        </div>
                        <!-- /.download-img -->
                        <div class="download-file-name">
                            <p><?php echo $file->post_title; ?></p>
                        </div>
                        <!-- /.download-file-name -->
                        <div class="download-btn">
                            <a href="<?php echo $file->guid; ?>"><?php echo __('Download'); ?></a>
                        </div>
                        <!-- /.download-btn -->

                    </div>

                </div>
                <!-- /.container-sm -->
            </div>
            <!-- /.page-wrap -->

        <?php endwhile; ?>


    <!-- /.page-wrap -->
<?php

//}
    ?>


        <!-- /.social -->

   <!-- /.contacts -->

    </div>
    <!-- /.wrapper -->



<?php
get_footer();
