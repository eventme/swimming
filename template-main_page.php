<?php
/**
 * Template name: Main page
 *
 */
get_header();
?>


<?php
$pages = get_pages(array(
    'meta_key' => '_wp_page_template',
    'meta_value' => 'template-main_page.php'
));
$page_id = '';
foreach ($pages as $page) {
    $page_id = $page->ID;

}
?>
    <div class="main-slider-wrap swiper-container">
        <div class="swiper-wrapper">


            <?php
            $sliders = carbon_get_post_meta($page_id, 'crb_swim_slider');
            foreach ($sliders as $slider) {
                ?>


                <div class="swiper-slide">
                    <!--                    <div class="slider-logo">-->
                    <!--                        <img src="-->
                    <?php //echo carbon_get_post_meta($page_id, 'crb_swim_club_logo'); ?><!--" alt="image">-->
                    <!--                    </div>-->
                    <!--                    <div class="description-slide">-->
                    <!--                        <strong>--><?php //echo $slider['crb_slider_text_1']; ?><!--</strong>-->
                    <!--                        <span>--><?php //echo $slider['crb_slider_text_2']; ?><!--</span>-->
                    <!--                    </div>-->
                    <div class="main-slider-img">
                        <img class="main-img" src="<?php echo $slider['crb_swim_foto']; ?>" alt="image">
                        <div class="description-slide-img">
                            <p>
                                <?php echo $slider['crb_slider_text_1']; ?>
                            </p>
                        </div>
                        <!-- /.description-slide-img -->
                    </div>
                    <!-- /.main-slider-img -->

                </div>

            <?php }
            ?>


        </div>
        <div class="swiper-button-prev">
            <img src="<?php echo bloginfo('template_url') ?>/assets/img/arrow-left.svg" alt="image">
        </div>
        <div class="swiper-button-next">
            <img src="<?php echo bloginfo('template_url') ?>/assets/img/arrow-right.svg" alt="image">
        </div>
    </div>


    <div class="description-swimming-club">
        <div class="container">
            <!-- /.container -->
            <div class="description-item-wrap">
                <?php $blocks = carbon_get_post_meta($page_id, 'crb_swim_block');
                foreach ($blocks as $block) { ?>
                    <div class="description-item">
                        <a href="<?php echo $block['crb_block_link']; ?>">
                            <img class='description-img' src="<?php echo $block['crb_block_foto']; ?>" alt="image">
                            <h4><?php echo $block['crb_block_title']; ?></h4>
                            <p><?php echo $block['crb_block_text']; ?></p>
                        </a>
                    </div>

                <?php } ?>
            </div>
        </div>
    </div>

    <div class="latest-news-wrap">
        <div class="container">
            <div class="latest-news-items">
                <div class="latest-news">
                    <h2><span>Latest news</span></h2>
                    <div class="news-wrap">
                        <?php
                        $args = array(
                            'post_type' => 'post',
                            'post_per_page' => '3',
                        );
                        $query = new WP_Query($args);

                        if ($query->have_posts()) {
                            // The 2nd Loop
                            while ($query->have_posts()) {
                                $query->the_post(); ?>
                                <div class="news-item">
                                    <a class="news-link" href="<?php echo get_post_permalink(get_the_ID()) ?>"></a>
                                    <div class="news-thumbnail">
                                        <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="image">
                                    </div>
                                    <!-- /. -->

                                    <h3 class="caption-news">
                                        <a href="#"><?php the_title(); ?></a>
                                    </h3>
                                    <p class="text-news">
                                        <?php echo get_the_excerpt(get_the_ID()); ?>
                                    </p>
                                    <a href="<?php echo get_post_permalink(get_the_ID()) ?>" class="read-more">
                                        read more
                                    </a>
                                    <!-- /.read-more -->
                                    <span class="date-news"><?php echo get_the_date('Y-m-d'); ?></span>
                                </div>

                            <?php }

                            wp_reset_postdata();
                        }

                        ?>
                    </div>
                    <!-- /.news-wrap -->

                    <!-- /.news-item -->
                </div>
                <div class="sidebar-right">
                    <p>Follow us</p>
                    <a target="_blank" href="https://www.facebook.com/Swanswimmingclub/">
                        <img src="<?php echo bloginfo('template_url') ?>/assets/img/facebook-btn.png" alt="image">
                    </a>
                    <iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fmyswimpro%2Fvideos%2F2021871964701668%2F%3Fhc_ref%3DARTnrDWQENLK0dsRN7iy80c6uq3XCjZmhhdrNmwbJvl29n207BdMZ8IXrZBA2phuwV4&width=365&show_text=true&height=322&appId"
                            width="365" height="400" style="border:none;overflow:hidden" scrolling="no" frameborder="0"
                            allowTransparency="true"></iframe>
                </div>

            </div>
            <!-- /.latest-news -->

            <!-- /.sidebar-right -->

            <!-- /.latest-news -->
        </div>
        <!-- /.container -->
    </div>

    <div class="swiper-container masonry-gallery">
        <div class="swiper-wrapper masonry-wrapper swiper-no-swiping">

            <?php $images = carbon_get_post_meta($page_id, 'crb_masonry_gallery');
            $count = 1;
            foreach ($images as $image): ?>

                <?php $image_src = wp_get_attachment_image_src($image, 'full')[0]; ?>

                <?php if ($count == 1 || $count == 7 || $count == 13) { ?>
                    <div class="swiper-slide">
                        <div class="masonry-item">
                            <a class="link-popup" href="<?php echo $image_src; ?>"><img
                                        class='grid-item--width4 grid-item--height4' data-u="image"
                                        src="<?php echo $image_src; ?>"/></a>
                        </div>
                    </div>
                <?php } ?>

                <?php if ($count == 2 || $count == 8 || $count == 14) { ?>
                    <div class="swiper-slide group-slide-column">
                    <div class="masonry-item">
                        <a class="link-popup" href="<?php echo $image_src; ?>"><img
                                    class='grid-item--width2 grid-item--height2' data-u="image"
                                    src="<?php echo $image_src; ?>"/></a>
                    </div>
                <?php } ?>

                <?php if ($count == 3 || $count == 9 || $count == 15) { ?>
                    <div class="masonry-item">
                        <a class="link-popup" href="<?php echo $image_src; ?>"><img
                                    class='grid-item--width2 grid-item--height2' data-u="image"
                                    src="<?php echo $image_src; ?>"/></a>
                    </div>
                    </div>
                <?php } ?>

                <?php if ($count == 4 || $count == 10 || $count == 16) { ?>
                    <div class="swiper-slide group-slide-column">
                    <div class="masonry-item">
                        <a class="link-popup" href="<?php echo $image_src; ?>"><img
                                    class='grid-item--width3 grid-item--height3' data-u="image"
                                    src="<?php echo $image_src; ?>"/></a>
                    </div>
                <?php } ?>

                <?php if ($count == 5 || $count == 11 || $count == 17){ ?>
                <div class="group-slide-row">
                <div class="masonry-item">
                    <a class="link-popup" href="<?php echo $image_src; ?>"><img
                                class='grid-item--width2 grid-item--height2' data-u="image"
                                src="<?php echo $image_src; ?>"/></a>
                </div>
            <?php } ?>

                <?php if ($count == 6 || $count == 12 || $count == 18) { ?>
                    <div class="masonry-item">
                        <a class="link-popup" href="<?php echo $image_src; ?>"><img
                                    class='grid-item--width2 grid-item--height2' data-u="image"
                                    src="<?php echo $image_src; ?>"/></a>
                    </div>
                    </div>
                    </div>
                <?php } ?>

                <?php $count++; endforeach; ?>

        </div>
    </div>

    <div class="link-join-us">
        <a href="<?php echo carbon_get_post_meta(get_the_ID(), 'crb_link_url'); ?>">
            <?php echo carbon_get_post_meta(get_the_ID(), 'crb_link_title'); ?>
        </a>
    </div>
    <!-- /.link-join-us -->

<?php
get_footer();
