<?php
/**
 * Template name: Join Us
 *
 */

get_header(); ?>

<?php while (have_posts()) : the_post(); ?>

    <div class="wrapper">
        <div class="page-wrap join-us">
            <div class="container">
                <div class="feedback-swimming">
                    <h2><?php the_title(); ?></h2>
                    <?php the_content(); ?>
                </div>
                <!-- /.feedback-swimming -->
                <?php echo do_shortcode('[contact-form-7 id="13" title="Contact form 1"]'); ?>

            </div>
            <!-- /.container-sm -->
        </div>
        <!-- /.page-wrap -->
    </div>

<?php endwhile; ?>

<?php get_footer();